package info.earty.image.presentation;

import info.earty.presentation.CxfServiceApi;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Encoding;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.apache.cxf.jaxrs.ext.multipart.Attachment;

import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;

@Tag(name = "image")
@Path("/image/command/")
public interface ImageCommandApi extends CxfServiceApi {

    @POST
    @Path("/create")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Operation(requestBody = @RequestBody(content = @Content(
            mediaType = "multipart/form-data",
            schema = @Schema(implementation = CreateImageRequest.class),
            encoding = {
                    @Encoding(name="file", contentType = "image" + "/" + MediaType.MEDIA_TYPE_WILDCARD),
            }
    )))
    void create(@Parameter(hidden = true) Attachment attachment, @QueryParam("workingCardId") String workingCardId);

    class CreateImageRequest {
        @Schema(format = "binary")
        public String file;
    }

}
