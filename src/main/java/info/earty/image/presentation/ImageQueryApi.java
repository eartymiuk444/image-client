package info.earty.image.presentation;

import info.earty.presentation.CxfServiceApi;
import io.swagger.v3.oas.annotations.tags.Tag;

import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

@Tag(name = "image")
@Path("/image/query/")
public interface ImageQueryApi extends CxfServiceApi {
    @GET
    @Path("/{imageId}")
    @Produces(MediaType.WILDCARD)
    Response get(@PathParam("imageId") String imageId);

    @GET
    @Path("/published/{imageId}")
    @Produces(MediaType.WILDCARD)
    Response getPublished(@PathParam("imageId") String imageId, @QueryParam("workingCardId") String workingCardId);
 }
