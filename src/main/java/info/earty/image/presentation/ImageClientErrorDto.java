package info.earty.image.presentation;

import lombok.Data;

@Data
public class ImageClientErrorDto {
    private String message;
}
